@extends('layout')

@section('konten')
    <!-- ======= About Section ======= -->
    <section id="about" class="about-mf sect-pt4 route">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <div class="box-shadow-full">
                <div class="row">
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-sm-6 col-md-5">
                        <div class="about-img">
                          <img src="assets/img/profil.png" class="img-fluid rounded b-shadow-a" alt="">
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-7">
                        <div class="about-info">
                          <p><span class="title-s">Name: </span> <span>Rama Mahardika</span></p>
                          <p><span class="title-s">Profile: </span> <span>Mahasiswa</span></p>
                          <p><span class="title-s">Email: </span> <span>rama.mahardika@undiksha.ac.id</span></p>
                          <p><span class="title-s">Phone: </span> <span>(+62) 87856720189</span></p>
                        </div>
                      </div>
                    </div>
                    <div class="skill-mf">
                      <p class="title-s">Skill</p>
                      <span>HTML</span> <span class="pull-right">65%</span>
                      <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 65%;" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                      <span>PHP</span> <span class="pull-right">50%</span>
                      <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                      <span>PYTHON</span> <span class="pull-right">55%</span>
                      <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 55%" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="about-me pt-4 pt-md-0">
                      <div class="title-box-2">
                        <h5 class="title-left">
                          About me
                        </h5>
                      </div>
                      <p class="lead">
                        Jenjang  Pendidikan:
                      </p>
                      <p class="lead">
                        SD – SD Negeri 6 Pejarakan, 2007-2013
                      </p>
                      <p class="lead">
                        SMP – SMP Negeri 2 Gerokgak, 2013-2016
                      </p>
                      <p class="lead">
                        SMA – SMA Negeri 2 Gerokgak, 2016-2019
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section><!-- End About Section -->
@endsection