@extends('layout')

@section('konten')
 <!-- ======= Portfolio Section ======= -->
 <section id="work" class="portfolio-mf sect-pt4 route">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="title-box text-center">
          <h3 class="title-a">
            Portfolio
          </h3>
          <p class="subtitle-a">
            the experience that I did during college.
          </p>
          <div class="line-mf"></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="work-box">
          <a href="assets/img/work-1.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox">
            <div class="work-img">
              <img src="assets/img/HTML.png" alt="" class="img-fluid">
            </div>
          </a>
          <div class="work-content">
            <div class="row">
              <div class="col-sm-8">
                <h2 class="w-title">HTML</h2>
                <div class="w-more">
                  <span class="w-ctegory">Web Design</span> / <span class="w-date">2021</span>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="w-like">
                  <a href="portfolio-details.html"> <span class="bi bi-plus-circle"></span></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="work-box">
          <a href="assets/img/work-2.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox">
            <div class="work-img">
              <img src="assets/img/PHP.png" alt="" class="img-fluid">
            </div>
          </a>
          <div class="work-content">
            <div class="row">
              <div class="col-sm-8">
                <h2 class="w-title">PHP</h2>
                <div class="w-more">
                  <span class="w-ctegory">Web Design</span> / <span class="w-date">2021</span>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="w-like">
                  <a href="portfolio-details.html"> <span class="bi bi-plus-circle"></span></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="work-box">
          <a href="assets/img/work-3.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox">
            <div class="work-img">
              <img src="assets/img/PYTHON.png" alt="" class="img-fluid">
            </div>
          </a>
          <div class="work-content">
            <div class="row">
              <div class="col-sm-8">
                <h2 class="w-title">PYTHON</h2>
                <div class="w-more">
                  <span class="w-ctegory">Web Design</span> / <span class="w-date">2021</span>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="w-like">
                  <a href="portfolio-details.html"> <span class="bi bi-plus-circle"></span></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="work-box">
</section><!-- End Portfolio Section -->
@endsection