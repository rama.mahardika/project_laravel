<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route ::get('/layout',[App\Http\Controllers\LayoutController :: class,'layout']);
Route ::get('/about',[App\Http\Controllers\AboutController :: class,'about']);
Route ::get('/work',[App\Http\Controllers\WorkController :: class,'work']);
Route ::get('/contact',[App\Http\Controllers\ContactController :: class,'contact']);